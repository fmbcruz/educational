<?php

// Require da classe de conexão
require ('database.php');

// Instancia Conexão PDO
$conn = Database::getInstance();

function insert($table = null, $values = [])
{
    $conn = Database::getInstance();
    $sql = 'INSERT INTO '. $table . addCampo($values);
    $stm = $conn->prepare($sql);
    addCampo($values, $stm);
    $stm->execute() OR die(print_r($stm->errorInfo()));
    return $conn->lastInsertId();
}

function update($table = null, $columns = null, $values = [], $where = null){
    $conn = Database::getInstance();
    $sql = 'UPDATE '. $table . ' SET ' . $columns . ' WHERE ' . $where;
    $stm = $conn->prepare($sql);
    addCampo($values, $stm);
    $stm->execute() OR die(print_r($stm->errorInfo()));
    return ($stm);
}

function delete($table = null, $where = null){

    $conn = Database::getInstance();
    $sql = 'DELETE FROM '. $table . ' WHERE ' . $where;

    $stm = $conn->prepare($sql);
    $stm->execute() OR die(print_r($stm->errorInfo()));
    return $stm;
}

function select($table = null, $values = null, $extra = null)
{
    $conn = Database::getInstance();
    $rows = [];

    $sql = 'SELECT ' . $values . ' FROM ' . $table;
    
    if(!empty($extra)):
        $sql = $sql . $extra;
    endif;

    $stm = $conn->prepare($sql);
    $stm->execute() OR die(print_r($stm->errorInfo()));

    while($row = $stm->fetch(PDO::FETCH_OBJ)):
        $rows[] = $row;
    endwhile;

    return $rows;
}

function addCampo($values = [], $stm = []){
    if(empty($stm)):
        $col = ' (';
        $els = ' VALUES (';
        foreach($values as $key => $val):
            $col .= $key . ','; 
            $els .= '?,';
        endforeach;
        $col = rtrim($col, ',') . ')';
        $els = rtrim($els, ',') . ')';
        
        return $col . $els;
    else:
        $n = 1;
        foreach($values as $key => $val):
            $stm->bindValue($n, $val);
            $n++;
        endforeach;
        
        return $stm;
    endif;
}