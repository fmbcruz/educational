<?php

/**
 * @ Prepare routes in project
 * @ author: Filipe Cruz
 * @ email: fmbcruz@live.com
 */

    /**
     * @return string
     */
    function getView(){

        /**
         * @ urlActual = Actual url accessed
         */
        $urlActual = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        /**
         * @ urlParts  = Array with slugs of url
         * @ urlSize   = Number of elements in urlParts
         */
        $urlParts  = explode('/', $urlActual);

        $getView = 'views/index.phtml';

//            echo '<pre>';
//            print_r($urlParts);
//            echo '</pre>';

        /**
         * @ Url default of module
         */


        if(empty($urlParts[3])):
            $getView = 'views/index.phtml';
        elseif(!empty($urlParts[3]) && count($urlParts) <= 5):
            $getView = 'views/' . $urlParts[3] . '.phtml';
        elseif(isset($urlParts[5]) == 'php'):
            $getView = 'app/php/' . $urlParts[6];
        endif;


//        /**
//         * @ Case file not found, will show 404 page
//         */
//        if(!is_file($getView)):
//            $getView ='views/error.phtml';
//        endif;

        return $getView;
    }


