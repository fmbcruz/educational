$('document').ready(function(){
    /*
        @ URL cursos
     */
    var url = 'http://educational.edu/app/php/temas.php';

    /*
    Get category
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getCategory'},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            $.each(rsp.data, function(key, val){
                $("#category").append('<option value="'+ val.id +'">'+ val.name +'</option>');
            });
        } else {
            alert(rsp.message)
        }
    });

    /*
        Include new Concourse
     */
    $("#newTheme").click(function(){

        var theme = $("#formTheme #theme").val(),
            material = $("#formTheme #material").val(),
            resume = $("#formTheme #resume").val();

            $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod:'addTheme',
                    theme: theme,
                    material: material,
                    resume: resume
                 },
            dataType: 'json'
        }).done(function(rsp) {
            $("#formTheme .alert span").html(rsp.message);
            $("#formTheme .alert").removeAttr('hidden');
            $("#formTheme").removeAttr('disabled');
            if(!rsp.success){
                $("#formTheme .alert").addClass("alert-danger");
                $("#formTheme .alert span").html(rsp.message);
                $("#formTheme .alert").removeAttr('hidden');
            }
            else{
                $("#formTheme .alert").addClass("alert-success");
                setTimeout(function(){
                    $(".close").click();
                }, 500);
                location.reload();
            }
        });

    });

    /*
        Get themes
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getThemes'},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
                var dataSet = [];
                $.each(rsp.data, function (key, val) {

                    var classe = null,
                        note = val.note,
                        corrects = val.corrects,
                        incorrects = val.incorrects;

                    if(incorrects == 0 && corrects == 0){
                        corrects = '-';
                        incorrects = '-';
                        note = '-';
                    } else if(val.note <= 65){
                        classe = 'alert-nota';
                    }

                    dataSet.push([
                        val.theme,
                        val.category,
                        val.material,
                        val.duration,
                        corrects,
                        incorrects,
                        '<span class="'+classe+'">'+note+'</span>'
                    ]);
                });

                tableStudy = $('#tableThemes').DataTable({
                    data: dataSet,
                    "order": [
                        [0, "desc"]
                    ],
                    "columnDefs": [
                        {"className": "dt-center", "targets": [1, 2, 3, 4, 5, 6]}
                    ],
                    columns: [
                        {title: "Tema"},
                        {title: "Categoria"},
                        {title: "Material"},
                        {title: "Duração"},
                        {title: "Acertos"},
                        {title: "Erros"},
                        {title: "Nota (%)"}
                    ]
                });
            }
        });



    /*
        @ Get all materials
     */
    $("#category").change(function(){


        $("#material").empty();

        $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod: 'getMaterial', material: $(this).val()},
            dataType: 'json'
        }).done(function(rsp){
            if(rsp.success){
                $.each(rsp.data, function(key, val){
                    $("#material").append('<option value="'+ val.id +'">'+ val.name +'</option>');
                });
            } else {
                alert(rsp.message)
            }
        });

        $("#material").removeAttr('disabled');

    });

    setTimeout(function(){
        $('#tableThemes').DataTable();
    }, 400);

});


function editValue(){

    tableThemes

}