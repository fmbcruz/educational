/*
    @ URL concursos
 */
var url = 'http://educational.edu/app/php/concursos.php',
    tableStudy = [];

$('document').ready(function(){
    $.fn.dataTable.ext.errMode = 'none'; // comment for show error message

    /*
        SELECT concourses
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'select'},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            $.each(rsp.data, function(key, val){
                var dataSet = [];
                $.each(rsp.data, function(key, val){
                    dataSet.push([
                        val.company,
                        val.job,
                        val.dh_subscription,
                        val.dh_exam,
                        val.salary,
                        '<td class="text-center">'+
                                    '<button class="btn btn-xs btn-success" id="edit" data-toggle="modal" data-target="#addConcourse" data-id="'+ val.id +'"><i class="fa fa-pencil"></i></button>'+
                                    '<button class="btn btn-xs btn-danger" id="delete" data-id="'+ val.id +'"><i class="fa fa-trash"></i></button>'+
                                    '<button class="btn btn-xs btn-info" id="info" data-id="'+ val.id +'"><i class="fa fa-bookmark"></i></button>'+
                                '</td>'+
                            '</tr>'
                    ]);
                });
                tableStudy = $('#tableConcourses').DataTable({
                    data: dataSet,
                    "order": [
                        [0, "asc"]
                    ],
                    "columnDefs": [
                        {"className": "dt-center", "targets": [2,3,4,5]}
                    ],
                    columns: [
                        { title: "Nome" },
                        { title: "Cargo" },
                        { title: "Inscrição" },
                        { title: "Prova" },
                        { title: "Salário" },
                        { title: "Ações" }
                    ]
                });
            });
        } else {
            alert(rsp.message)
        }
    });
});

$(document).on("click", '#new', function(event){
    $(".btn-concourse").attr('id','newConcourse');
    $('#id').val('');
    $('#company').val('');
    $('#examBoard').val('');
    $('#dateSubscription').val('');
    $('#dateExam').val('');
    $('#hourExam').val('');
    $('#subscriptionValue').val('');
    $('#job').val('');
    $('#salary').val('');
    $('#infoURL').val('');
    $("#subscription").prop('checked', false);
});

$(document).on("click", '#newConcourse', function(event){
    /*
        Include new Concourse
     */
    var company           = $("#formConcourse #company").val(),
        dateSubscription  = $("#formConcourse #dateSubscription").val(),
        dateExam          = $("#formConcourse #dateExam").val(),
        hourExam          = $("#formConcourse #hourExam").val(),
        subscriptionValue = $("#formConcourse #subscriptionValue").val(),
        subscription      = $("#formConcourse #subscription").val(),
        job               = $("#formConcourse #job").val(),
        salary            = $("#formConcourse #salary").val(),
        examBoard         = $("#formConcourse #examBoard").val(),
        infoURL           = $("#formConcourse #infoURL").val(),
        subscription      = $("#subscription").prop("checked");

    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod:'insert',
            company: company,
            dateSubscription: dateSubscription,
            dateExam: dateExam,
            hourExam: hourExam,
            subscription: subscription,
            subscriptionValue: subscriptionValue,
            job: job,
            salary: salary,
            examBoard: examBoard,
            infoURL: infoURL
        },
        dataType: 'json',
        beforeSend: function()
        {
            $("#saveMaterial").attr('disabled', true);
        }
    }).done(function(rsp) {
        $("#formConcourse .alert span").html(rsp.message);
        $("#formConcourse .alert").removeAttr('hidden');
        $("#formConcourse").removeAttr('disabled');
        if(!rsp.success){
            $("#formConcourse .alert").addClass("alert-danger");
            $("#formConcourse .alert span").html(rsp.message);
            $("#formConcourse .alert").removeAttr('hidden');
        }
        else{
            $("#formConcourse .alert").addClass("alert-success");
            setTimeout(function(){
                $(".close").click();
                location.reload();
            }, 2000);
        }
    });
});

$(document).on("click", '#saveConcourse', function(event){
    /*
        Save Concourse edited
     */
    var id                = $("#formConcourse #id").val(),
        company           = $("#formConcourse #company").val(),
        dateSubscription  = $("#formConcourse #dateSubscription").val(),
        dateExam          = $("#formConcourse #dateExam").val(),
        hourExam          = $("#formConcourse #hourExam").val(),
        subscriptionValue = $("#formConcourse #subscriptionValue").val(),
        job               = $("#formConcourse #job").val(),
        salary            = $("#formConcourse #salary").val(),
        examBoard         = $("#formConcourse #examBoard").val(),
        infoURL           = $("#formConcourse #infoURL").val(),
        subscription      = $("#subscription").prop("checked");

    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod:'update',
            id: id,
            company: company,
            dateSubscription: dateSubscription,
            dateExam: dateExam,
            hourExam: hourExam,
            subscription: subscription,
            subscriptionValue: subscriptionValue,
            job: job,
            salary: salary,
            examBoard: examBoard,
            infoURL: infoURL
        },
        dataType: 'json',
        beforeSend: function()
        {
            $("#saveMaterial").attr('disabled', true);
        }
    }).done(function(rsp) {

        $("#formConcourse .alert span").html(rsp.message);
        $("#formConcourse .alert").show();
        if(!rsp.success){
            $("#formConcourse .alert").addClass("alert-danger");
        }
        else{
            $("#formConcourse .alert").addClass("alert-success");
            setTimeout(function(){
                $(".close").click();
                location.reload();
            }, 1500);
        }
    });
});


$(document).on("click", '#edit', function (event) {

    $(".btn-concourse").attr('id','saveConcourse');
    $(".alert").hide();
    var id = $(this).attr('data-id');
    /*
        Return concourses for edit
    */

    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'select', id:id},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            $.each(rsp.data, function(key, val){
                $('#id').val(val.id);
                $('#company').val(val.company);
                $('#examBoard').val(val.exam_board);
                $('#dateSubscription').val(val.dh_subscription_edit);
                $('#dateExam').val(val.dh_exam_edit);
                $('#hourExam').val(val.hour_exam);
                $('#subscriptionValue').val(val.subscription_value);
                $('#job').val(val.job);
                $('#salary').val(val.salary);
                $('#infoURL').val(val.url);
                if(val.subscription == 1){
                    $("#subscription").prop('checked', true);
                } else {
                    $("#subscription").prop('checked', false);
                }
            });
        } else {
            $('#startStudy').show();
        }
    });

});



$(document).on("dblclick", '#delete', function (event) {

    var id = $(this).attr('data-id');
    /*
        Return concourses for edit
    */

    $.ajax({
        url: url,
        type: 'POST',
        data: {txtMethod: 'delete', id: id},
        dataType: 'json'
    }).done(function (rsp) {
        if (rsp.success) {
            alert(rsp.message);
            setTimeout(function(){
                location.reload();
            }, 500);
        } else {
            alert(rsp.message);
        }
    });
});

