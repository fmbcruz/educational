$('document').ready(function(){
    /*
        @ URL cursos
     */
    var url = 'http://educational.edu/app/php/index.php';

    var tableStudy = [];

    /*
    @ Get all Categories, materials and themes
*/
    function getAllTypes(txtMethod, value, name){
        $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod: txtMethod, material: value},
            dataType: 'json'
        }).done(function(rsp){
            if(rsp.success){
                $.each(rsp.data, function(key, val){
                    $('#'+name).append('<option value="'+ val.id +'">'+ val.name +'</option>');
                });
            } else {
                alert(rsp.message)
            }
        });
    }

    /*
        Get category
    */
    getAllTypes('getCategory', null, 'categoryShow');


    /*
        Get all studies of category selected
    */
    /*
            Get all studies of category selected
        */
    $("#categoryShow").change(function(){

        $(".alert").hide();
        var category = $("#categoryShow").val();

        $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod: 'getAllStudiesOfCategory', category: category},
            dataType: 'json'
        }).done(function(rsp){
            if(rsp.success){
                var dataSet = [];
                $.each(rsp.data, function(key, val){

                    var note = val.note,
                        corrects = val.corrects,
                        incorrects = val.incorrects;

                    if(incorrects == 0 && corrects == 0){
                        corrects = '-';
                        incorrects = '-';
                        note = '-';
                    }

                    dataSet.push([
                        val.theme,
                        val.material,
                        val.dh_start,
                        val.duration,
                        corrects,
                        incorrects,
                        note
                    ]);
                });

                tableStudy.destroy();
                $('#tableStudies').empty();

                tableStudy = $('#tableStudies').DataTable({
                    data: dataSet,
                    "order": [
                        [2, "desc"]
                    ],
                    "columnDefs": [
                        {"className": "dt-center", "targets": [2,3,4,5,6]}
                    ],
                    columns: [
                        { title: "Tema" },
                        { title: "Matéria" },
                        { title: "Data" },
                        { title: "Duração" },
                        { title: "Acertos" },
                        { title: "Erros" },
                        { title: "Nota (%)" }
                    ]
                });

            } else {
                $(".alert").addClass("alert-danger").show();
                $(".alert span").html(rsp.message);
                setTimeout(function(){
                    $(".alert").hide();
                }, 5000);
            }
        });

    });

    /*
        Get All Studies
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getAllStudies'},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            var dataSet = [];
            $.each(rsp.data, function(key, val){

                var note = val.note,
                    corrects = val.corrects,
                    incorrects = val.incorrects;

                if(incorrects == 0 && corrects == 0){
                    corrects = '-';
                    incorrects = '-';
                    note = '-';
                }


                dataSet.push([
                    val.theme,
                    val.material,
                    val.dh_start,
                    val.duration,
                    corrects,
                    incorrects,
                    note
                ]);
            });

            tableStudy = $('#tableStudies').DataTable({
                data: dataSet,
                "order": [
                    [2, "desc"]
                ],
                "columnDefs": [
                    {"className": "dt-center", "targets": [2,3,4,5,6]}
                ],
                columns: [
                    { title: "Tema" },
                    { title: "Matéria" },
                    { title: "Data" },
                    { title: "Duração" },
                    { title: "Acertos" },
                    { title: "Erros" },
                    { title: "Nota (%)" }
                ]
            });
        }
    });

    function statistics(){

        $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod: 'statistics'},
            dataType: 'json'
        }).done(function(rsp){
            if(rsp.success){

                AllLabels = [];
                dataCor = [];
                dataInc = [];
                backCor = [];
                backInc = [];
                concourse = [];

                $.each(rsp.data.concourse, function(key, val){
                    AllLabels.push(val.company.substr(0, 15));
                    dataCor.push(parseFloat(val.correct_perc));
                    backCor.push('rgba(54, 162, 235, 0.6)');
                    dataInc.push(parseFloat(val.incorrect_perc));
                    backInc.push('rgba(255, 98, 131, 0.6)');
                });

                concourse.push([AllLabels, dataCor, backCor, dataInc, backInc]);

                setTimeout(function(){
                    var ctx = document.getElementById("chartConcourse");
                    var chartConcourse = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: concourse[0][0],
                            datasets: [{
                                label: 'Acertos',
                                data: concourse[0][1],
                                backgroundColor: concourse[0][2],
                                borderWidth: 2
                            },
                                {
                                    label: 'Erros',
                                    data: concourse[0][3],
                                    backgroundColor: concourse[0][4],
                                    borderWidth: 2
                                }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });

                    AllLabels = [];
                    dataCor = [];
                    dataInc = [];
                    backCor = [];
                    backInc = [];
                    material = [];

                    $.each(rsp.data.material, function(key, val){
                        AllLabels.push(val.name.substr(0, 15));
                        dataCor.push(parseFloat(val.correct_perc));
                        backCor.push('rgba(54, 162, 235, 0.6)');
                        dataInc.push(parseFloat(val.incorrect_perc));
                        backInc.push('rgba(255, 98, 131, 0.6)');

                    });

                    material.push([AllLabels, dataCor, backCor, dataInc, backInc]);

                    setTimeout(function(){
                        var ctx = document.getElementById("chartMaterials");
                        var chartMaterials = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: material[0][0],
                                datasets: [{
                                    label: 'Acertos',
                                    data: material[0][1],
                                    backgroundColor: material[0][2],
                                    borderWidth: 2
                                },
                                    {
                                        label: 'Erros',
                                        data: material[0][3],
                                        backgroundColor: material[0][4],
                                        borderWidth: 2
                                    }]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }]
                                }
                            }
                        });

                        dataCor = 0;
                        dataInc = 0;
                        LabelCor = 'Acertos';
                        LabelInc = 'Erros';
                        Total = 0;
                        all = [];

                        $.each(rsp.data.all, function(key, val){
                            dataCor = val.correct_perc;
                            dataInc = val.incorrect_perc;
                            LabelCor = LabelCor + ' (' + val.total_correct + ')';
                            LabelInc = LabelInc + ' (' + val.total_incorrect + ')';
                            Total = (parseInt(val.total_correct) + parseInt(val.total_incorrect));

                        });

                        all.push([AllLabels, dataCor, backCor, dataInc, backInc]);

                        console.log(all);

                        setTimeout(function(){
                            var ctx = document.getElementById("chartExercises");
                            var chartAll = new Chart(ctx, {
                                type: 'pie',
                                data: {
                                    labels: [LabelInc, LabelCor],
                                    datasets: [{
                                        data: [dataInc, dataCor],
                                        backgroundColor: ["rgb(255, 98, 131)", 'rgb(54, 162, 235)']
                                    }]
                                }
                            });

                            // $("#total_exercises").html(Total);

                            AllLabels = [];
                            dataCor = [];
                            dataInc = [];
                            backCor = [];
                            backInc = [];
                            categories = [];

                            $.each(rsp.data.categories, function(key, val){
                                AllLabels.push(val.name.substr(0, 15));
                                dataCor.push(parseFloat(val.correct_perc));
                                backCor.push('rgba(54, 162, 235, 0.6)');
                                dataInc.push(parseFloat(val.incorrect_perc));
                                backInc.push('rgba(255, 98, 131, 0.6)');

                            });

                            categories.push([AllLabels, dataCor, backCor, dataInc, backInc]);

                            setTimeout(function(){
                                var ctx = document.getElementById("chartCategories");
                                var chartCategories = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: categories[0][0],
                                        datasets: [{
                                            label: 'Acertos',
                                            data: categories[0][1],
                                            backgroundColor: categories[0][2],
                                            borderWidth: 2
                                        },
                                            {
                                                label: 'Erros',
                                                data: categories[0][3],
                                                backgroundColor: categories[0][4],
                                                borderWidth: 2
                                            }]
                                    },
                                    options: {
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero:true
                                                }
                                            }]
                                        }
                                    }
                                });
                                }, 500);



                        }, 500);

                    }, 500);

                }, 500);

            } else {
                alert(rsp.message)
            }

        });
    }
    statistics();
});
