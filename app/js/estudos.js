/*
    @ URL cursos
 */
var url = 'http://educational.edu/app/php/estudos.php';

/*
        @ Get all Categories, materials and themes
    */
function getAllTypes(txtMethod, value, name){
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: txtMethod, material: value},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            $.each(rsp.data, function(key, val){
                $('#'+name).append('<option value="'+ val.id +'">'+ val.name +'</option>');
            });
        } else {
            alert(rsp.message)
        }
    });
}


$('document').ready(function(){
    var tableStudy = [];

    $("#review").Editor();
    $("#reviewEdit").Editor();

    /*
        Get concourses
     */
    getAllTypes('getConcourses', null, 'concourse');

    /*
        Get category
     */
    getAllTypes('getCategory', null, 'category');

    /*
       @ Get all materials
    */
    $("#category").change(function(){
        $("#material").empty().removeAttr('disabled');
        $('#material').append('<option value="">Selecione a matéria</option>');
        getAllTypes('getMaterial', $(this).val(), 'material');
    });

    /*
       @ Get themes
    */
    $("#material").change(function(){
        $("#theme").empty().removeAttr('disabled');
        $('#theme').append('<option value="">Selecione o tema</option>');
        getAllTypes('getTheme', $(this).val(), 'theme');
    });

    /*
       Verify if exists studies open
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getOpenStudies'},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            $.each(rsp.data, function(key, val){
                $('#concourse').val(val.concourse_id);
                $('#category').val(val.category_id);
                getAllTypes('getMaterial', val.category_id, 'material');
                $('#material').val(val.material_id).removeAttr('disabled');
                getAllTypes('getTheme', val.material_id, 'theme');
                $('#theme').val(val.theme_id).removeAttr('disabled');
                $(".Editor-editor").html(val.review);
                $("#corrects").val(val.corrects);
                $("#incorrects").val(val.incorrects);
                $('#idStudy').val(val.id);
                $('#startStudy').hide();
                $('#stopStudy').show();
            });
        } else {
            $('#startStudy').show();
        }
    });


    /*
        Start Study
     */

    $("#startStudy").click(function(){

        $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod: 'startStudy', theme: $('#theme').val()},
            dataType: 'json'
        }).done(function(rsp) {
            $("#formStudy .alert span").html(rsp.message);
            $("#formStudy .alert").removeAttr('hidden').removeAttr('disabled');
            if(!rsp.success){
                $("#formStudy .alert").addClass("alert-danger");
                $('#startStudy').hide();
                $('#stopStudy').show();
                setTimeout(function(){
                    $(".close").click();
                }, 500);
            }
            else{
                location.reload();
                $("#formStudy .alert").addClass("alert-success");
                setTimeout(function(){
                    $(".close").click();
                }, 5000);
            }
        });
    });

    /*
        Stop Study
     */

    $("#stopStudy").click(function(){

        $('#stopStudy').attr('disabled', true);

        var id         = $("#idStudy").val(),
            theme      = $("#theme").val(),
            comments   = $("#comments").val(),
            review     = $(".Editor-editor").html(),
            concourse  = $("#concourse").val(),
            corrects   = $("#corrects").val(),
            incorrects = $("#incorrects").val(),
            save       = $("#stopStudy").attr('data-save');


        $.ajax({
            url  : url,
            type : 'POST',
            data : {
                txtMethod: 'stopStudy',
                id: id,
                theme: theme,
                concourse: concourse,
                comments: comments,
                review: review,
                corrects: corrects,
                incorrects: incorrects,
                save: save

            },
            dataType: 'json'
        }).done(function(rsp) {
            $("#formStudy .alert span").html(rsp.message);
            $("#formStudy .alert").removeAttr('hidden');
            $("#formStudy").removeAttr('disabled');
            if(!rsp.success){
                $("#formStudy .alert").addClass("alert-danger");
                $("#formStudy .alert span").html(rsp.message);
                $("#formStudy .alert").removeAttr('hidden');
                $('#stopStudy').attr('disabled', false);
            }
            else{
                location.reload();
                $("#formStudy .alert").addClass("alert-success");
                setTimeout(function(){
                    $(".close").click();
                }, 3000);
            }
        });

    });

    /*
     Get category
    */
    getAllTypes('getCategory', null, 'categoryShow');


    /*
        Get all studies of category selected
    */
    $("#categoryShow").change(function(){

        $(".alert").hide();
        var category = $("#categoryShow").val();

        $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod: 'getAllStudiesOfCategory', category: category},
            dataType: 'json'
        }).done(function(rsp){
            if(rsp.success){
                var dataSet = [];
                $.each(rsp.data, function(key, val){
                    dataSet.push([
                        '<a class="studies-list" id="'+val.id+'">'+ val.theme + '</a>',
                        val.material,
                        val.dh_start,
                        val.duration,
                        val.corrects,
                        val.incorrects,
                        val.note
                    ]);
                });

                tableStudy.destroy();
                $('#tableStudies').empty();

                tableStudy = $('#tableStudies').DataTable({
                    data: dataSet,
                    "order": [
                        [2, "desc"]
                    ],
                    "columnDefs": [
                        {"className": "dt-center", "targets": [2,3,4,5,6]}
                    ],
                    columns: [
                        { title: "Tema" },
                        { title: "Matéria" },
                        { title: "Duração" },
                        { title: "Acertos" },
                        { title: "Erros" },
                        { title: "Nota (%)" }
                    ]
                });

            } else {
                $(".alert").addClass("alert-danger").show();
                $(".alert span").html(rsp.message);
                setTimeout(function(){
                    $(".alert").hide();
                }, 5000);
            }
        });

    });

    /*
        Get All Studies
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getAllStudies'},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            var dataSet = [];
            $.each(rsp.data, function(key, val){

                var note = val.note,
                    corrects = val.corrects,
                    incorrects = val.incorrects;

                if(incorrects == 0 && corrects == 0){
                    corrects = '-';
                    incorrects = '-';
                    note = '-';
                }

                dataSet.push([
                    '<a class="studies-list" id="'+val.id+'">'+ val.theme + '</a>',
                    val.material,
                    val.dh_start,
                    val.duration,
                    corrects,
                    incorrects,
                    note
                ]);
            });

            tableStudy = $('#tableStudies').DataTable({
                data: dataSet,
                "order": [
                    [2, "desc"]
                ],
                "columnDefs": [
                    {"className": "dt-center", "targets": [2,3,4,5,6]}
                ],
                columns: [
                    { title: "Tema" },
                    { title: "Matéria" },
                    { title: "Data" },
                    { title: "Duração" },
                    { title: "Acertos" },
                    { title: "Erros" },
                    { title: "Nota (%)" }
                ]
            });
        }
    });

});

$(document).on("click", '.studies-list', function (event) {
    var studyId = $(this).attr('id');

    /*
        Return theme for edition
    */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getStudiesForEdition', id:studyId},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            $.each(rsp.data, function(key, val){
                $('#concourse').val(val.concourse_id);
                $('#category').val(val.category_id);
                getAllTypes('getMaterial', val.category_id, 'material');
                $('#material').val(val.material_id).removeAttr('disabled');
                getAllTypes('getTheme', val.material_id, 'theme');
                $('#theme').val(val.theme_id).removeAttr('disabled');
                $(".Editor-editor").html(val.review);
                $("#corrects").val(val.corrects);
                $("#incorrects").val(val.incorrects);
                $('#idStudy').val(val.id);
                $('#startStudy').hide();
                $('#stopStudy').show().attr('data-save',true);
            });
        } else {
            $('#startStudy').show();
        }
    });

    $('#stopStudy').html('<i class="fa fa-save"></i> Salvar').removeClass('btn-danger').addClass('btn-success');
    $('html, body').animate({
        scrollTop: $("#formStudy").offset().top
    }, 500);
});

$(document).on("keyup", '.Editor-container', function (event) {
   if(event.which === 13 || event.keyCode === 13){
       var id         = $("#idStudy").val(),
           review     = $(".Editor-editor").html();

       $.ajax({
           url: url,
           type: 'POST',
           data: {txtMethod: 'autoUpdateReview', id: id, review: review},
           dataType: 'json'
       }).done(function(rsp){
           if(rsp.success){
               $("#statusbar").append('<div class="label saving">Salvando...</div>');
               setTimeout(function () {
                   $(".saving").html('Salvo');
                   setTimeout(function(){
                       $(".saving").hide();
                   }, 3000);
               }, 3000)
           }
       });
   }
});