/*
    @ URL cursos
 */
var url = 'http://educational.edu/app/php/materias.php';

$('document').ready(function(){

    /*
        @ Functions
     */

    function removeSpaces(text){
       return text.replace(/\s+/g, '');
    }

    /*
        Get material relation
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getMainMaterials'},
        dataType: 'json'
    }).done(function(rsp){
        if(rsp.success){
            $.each(rsp.data, function(key, val){
                $("#relation").append('<option value="'+ val.id +'">'+ val.name +'</option>');
            });
        } else {
            alert(rsp.message)
        }
    });

    /*
        Get of all materials
     */
    $.ajax({
        url  : url,
        type : 'POST',
        data : {txtMethod: 'getMaterials'},
        dataType: 'json'
    }).done(function(rsp) {

        $("#saveMaterial").removeAttr('disabled');
        if(!rsp.status){
            $("#formMaterial .alert span").html(rsp.message);
            $("#formMaterial .alert").removeAttr('hidden');
            $("#formMaterial .alert").addClass("alert-danger");
        }
        else{

            $.each(rsp.data, function(key, val){

                if(val.material_id === null){

                    $("#materialsList").append('<a class="list-group-item" data-remote="true" href="#'+ val.id +'" id="'+ removeSpaces(val.name) +'"'+
                        ' data-toggle="collapse" data-parent="#'+ val.id +'" style="padding-left: 25px;">'+
                        '<span class="icon"><i class="fa fa-chevron-down fa-lg fa-fw"></i></span>'+
                        '<span style="margin-left: 25px;">'+ val.name +'</span> '+
                        '<span class="menu-ico-collapse"><span class="num-elements"></span></span></a>' +
                        '<div class="collapse list-group-submenu" id="'+ val.id +'">' +
                        '<div id="'+removeSpaces(val.name)+'_'+val.id+'"></div>' +
                        '</div>'
                    );

                    var n = 0;
                    $.each(rsp.data, function(subKey, subVal){
                        if(subVal.material_relation === val.id){
                            $('#'+removeSpaces(val.name)+'_'+val.id).append('<a href="#" id="subMaterial" data-id="'+subVal.id+'" class="list-group-item sub-item" style="padding-left: 78px;">'+ subVal.name+' <i class="fa fa-trash" id="delSubItem" data-id="'+ subVal.id +'"  data-toggle="modal" data-target="#delete"></i></a>');
                            n++;
                        }
                    });
                    if(n >= 1){
                        $('#'+removeSpaces(val.name)+' .num-elements').append(n);
                    } else {
                        $('#' + removeSpaces(val.name) + ' span').removeClass('num-elements');
                    }
                }

            });
        }
    });

    /*
        Include new Material
     */
    $("#saveMaterial").click(function(){

        var material = $("#material").val(),
            relation = $("#relation").val();

            $.ajax({
            url  : url,
            type : 'POST',
            data : {txtMethod: 'addMaterial', material: material, relation: relation},
            dataType: 'json',
            beforeSend: function()
            {
                $("#saveMaterial").attr('disabled', true);
            }
        }).done(function(rsp) {
            $("#formMaterial .alert span").html(rsp.message);
            $("#formMaterial .alert").removeAttr('hidden');
            $("#saveMaterial").removeAttr('disabled');
            if(!rsp.status){
                $("#formMaterial .alert").addClass("alert-danger");
            }
            else{
                $("#formMaterial .alert").addClass("alert-success");
                // setTimeout(function(){
                //     $(".close").click();
                // }, 500);
                // location.reload();
            }
        });

    });

    /*
        @ Delete Material
     */
    $(".delete").click(function(){
        alert($(this).attr('id'));
    });

    /*
        Alter chevron up or down of list
     */
    var n = 0;
    $(document).on("click", ".list-group-item", function(){
        var id = $(this).attr('id');
        if(n === 0){
            $('#'+id+' .icon').html('<i class="fa fa-chevron-up fa-lg fa-fw"></i>');
            n = 1;
        } else {
            $('#'+id+' .icon').html('<i class="fa fa-chevron-down fa-lg fa-fw"></i>');
            n = 0;
        }
    });
});


$(document).on("click", "#subMaterial", function(){
    var material = $(this).attr('data-id');

    $.ajax({
        url: url,
        type: 'POST',
        data: {txtMethod: 'studiesOfMaterial', material: material},
        dataType: 'json'
    }).done(function(rsp){
        $("#tableStudies tbody").empty();
        $.each(rsp.data, function(key, val){

            var total = parseInt(val.corrects) + parseInt(val.incorrects),
                perc_correct = ((val.corrects/total)*100).toFixed(1),
                perc_incorrect = ((val.incorrects/total)*100).toFixed(1);

            $("#tableStudies tbody").append('<tr>' +
                '<td>'+val.name+'</td>' +
                '<td class="text-center" data-toggle="tooltip" data-placement="top" title="'+perc_correct+'%">'+val.corrects+'</td>' +
                '<td class="text-center" data-toggle="tooltip" data-placement="top" title="'+perc_incorrect+'%"><span>'+val.incorrects+'</span></td>' +
                '<td class="text-center">'+(total)+'</td>' +
                '</tr>');

            if(perc_correct <= perc_incorrect){
                $("#tableStudies tbody span").addClass('alert-nota');
            }

        });

    });

    $("#panelStudies h3").html('Tópicos de ' + $(this).text());
    $("#panelStudies").show();
});


$(document).on("click", "#delSubItem", function(){
    var material_id = $(this).attr('data-id');
    $("#deleteMateria").attr('data-id', material_id);
});

$(document).on("click", '#deleteMateria', function (event) {

    var id = $(this).attr('data-id');

    $.ajax({
        url: url,
        type: 'POST',
        data: {txtMethod: 'delete', material: id},
        dataType: 'json'
    }).done(function (rsp) {
        if (rsp.success) {
            alert(rsp.message);
            setTimeout(function(){
                location.reload();
            }, 500);
        } else {
            alert(rsp.message);
        }
    });
});
