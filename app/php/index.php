<?php

/**
 *  index
 */

// Require da classe de conexão
require ('../../libs/sql.php');

// Method
$method = isset($_POST['txtMethod']) ? $_POST['txtMethod'] : "";
// Variables
$category = isset($_POST['category']) ? $_POST['category'] : "";

$return = [];

switch($method):

    case 'getAllStudies':
        // Select all studies
        $table = "studies s";
        $values = "s.id, t.name as theme, c.name as category, m.name as material, TIMEDIFF(dh_finish, dh_start) as duration, DATE_FORMAT(s.dh_start,'%d/%m/%Y %H:%i') as dh_start, s.corrects, s.incorrects, round((corrects/(corrects + incorrects)*100),1) as note";
        $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                   INNER JOIN materials m ON (t.material_id = m.id)
                   INNER JOIN material_relations r ON (m.id = r.material_id)
                   INNER join materials c ON (r.material_relation = c.id)
                   WHERE s.dh_finish IS NOT NULL";

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há estudos cadastrados.', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;

        break;
    case 'getAllStudiesOfCategory':
        // Select all concourses
        if(empty($category)):
            $return = ['success' => false, 'message' => 'Informe a categoria!'];
        else:
            $table = "studies s";
            $values = "s.id, t.name as theme, c.name as category, m.name as material, TIMEDIFF(dh_finish, dh_start) as duration, DATE_FORMAT(s.dh_start,'%d/%m/%Y %H:%i') as dh_start, s.corrects, s.incorrects, round((corrects/(corrects + incorrects)*100),1) as note";
            $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                       INNER JOIN materials m ON (t.material_id = m.id)
                       INNER JOIN material_relations r ON (m.id = r.material_id)
                       INNER join materials c ON (r.material_relation = c.id)
                       WHERE s.dh_finish IS NOT NULL AND c.id = " . $category;

            $rows = select($table, $values, $extra);

            if(count($rows) <= 0):
                $return = ['success' => false, 'message' => 'Não há estudos cadastrados para esta categoria.', 'data' => $rows];
            else:
                $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
            endif;
        endif;
        break;

    case 'getCategory':

        $rows = select('materials', 'id, name', ' WHERE id NOT IN (SELECT material_id FROM material_relations)');

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'statistics':

        $table = "studies s";
        $extra = " WHERE corrects > 0 AND incorrects > 0";
        $values = "SUM(corrects) AS total_correct,
                   SUM(incorrects) AS total_incorrect,
                   (SUM(corrects) + SUM(incorrects)) AS total,
                   round((SUM(corrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS correct_perc,
                   round((SUM(incorrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS incorrect_perc";


        $rows['all'] = select($table, $values, $extra);

        $values = "c.company,
                   SUM(corrects) AS total_correct,
                   SUM(incorrects) AS total_incorrect,
                   (SUM(corrects) + SUM(incorrects)) AS total,
                   round((SUM(corrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS correct_perc,
                   round((SUM(incorrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS incorrect_perc";

        $extra = " INNER JOIN concourses c ON (s.concourse_id = c.id)
                  WHERE corrects > 0 AND incorrects > 0 
                  GROUP BY c.company";

        $rows['concourse'] = select($table, $values, $extra);

        $values = "m.name,
                   SUM(corrects) AS total_correct,
                   SUM(incorrects) AS total_incorrect,
                   (SUM(corrects) + SUM(incorrects)) AS total,
                   round((SUM(corrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS correct_perc,
                   round((SUM(incorrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS incorrect_perc";

        $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                  INNER JOIN materials m ON (t.material_id = m.id)
                  WHERE corrects > 0 AND incorrects > 0
                  GROUP BY m.name";

        $rows['material'] = select($table, $values, $extra);

        $values = "c.name,
                   SUM(corrects) AS total_correct,
                   SUM(incorrects) AS total_incorrect,
                   (SUM(corrects) + SUM(incorrects)) AS total,
                   round((SUM(corrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS correct_perc,
                   round((SUM(incorrects)*100)/(SUM(corrects) + SUM(incorrects)),2) AS incorrect_perc";

        $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                   INNER JOIN materials m ON (t.material_id = m.id)
                   INNER JOIN material_relations r ON (m.id = r.material_id)
                   INNER JOIN materials c ON (r.material_relation = c.id)
                   WHERE corrects > 0 AND incorrects > 0
                   GROUP BY c.name";

        $rows['categories'] = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há dados cadastrados.', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Dados selecionados', 'data' => $rows];
        endif;



        break;

endswitch;

echo json_encode($return);
