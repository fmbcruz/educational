<?php

/**
 *  Cursos
 */

// Require da classe de conexão
require ('../../libs/sql.php');

// Method
$method = isset($_POST['txtMethod']) ? $_POST['txtMethod'] : "";
// Variables
$material = isset($_POST['material'])   ? $_POST['material'] : null;
$relation = isset($_POST['relation']) ? $_POST['relation'] : null;

$return = [];

switch ($method):
    case 'addMaterial':
        if(empty($material)):
            $return = ['status' => false, 'message' => 'Informe o curso!'];
            break;
        endif;
        // Add new material in  table @ materials
        $values = [
            'name' => $material,
        ];

        $id_material = insert('materials', $values);
        if(!empty($relation)):
            $values = [
                'material_id' => $id_material,
                'material_relation' => $relation,
                'sort'      => $id_material,
            ];
            insert('material_relations', $values);
        endif;
        $return = ['status' => true, 'message' => 'Curso incluído com sucesso!', 'id' => $id_material];
        break;
    case 'getMaterials':
        // Select all materials
        $rows = select('materials mat', 'mat.id, mat.name, rel.material_id, rel.material_relation',
        ' LEFT JOIN educational.material_relations rel ON (mat.id = rel.material_id) ORDER BY mat.id');

        if(count($rows) <= 0):
            $return = ['status' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['status' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;
    case 'getMainMaterials':

        $rows = select('materials', 'id, name', ' WHERE id NOT IN (SELECT material_id FROM material_relations)');

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'studiesOfMaterial':

        $table = 'studies s';
        $values = "t.name, s.corrects, s.incorrects";
        $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                  INNER JOIN materials m ON (t.material_id = m.id)
                  WHERE m.id = " . $material;

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há estudos nesta matéria', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Estudos selecionados.', 'data' => $rows];
        endif;
        break;
    case 'delete':
        if(empty($material)):
            $return = ['success' => false, 'message' => 'Informe o id!'];
        else:

            delete('material_relations', 'material_id = ' . $material);
            $resp = delete('materials', 'id = ' . $material);
            $return = ['success' => true, 'message' => 'Matéria apagado com sucesso!', 'id' => $resp];
        endif;
        break;

endswitch;

echo json_encode($return);

?>