<?php

/**
 *  Cursos
 */

// Require da classe de conexão
require ('../../libs/sql.php');

// Method
$method = isset($_POST['txtMethod']) ? $_POST['txtMethod'] : "";
// Variables
$material_id = isset($_POST['material'])   ? $_POST['material'] : null;
$theme       = isset($_POST['theme'])   ? $_POST['theme'] : null;
$resume      = isset($_POST['$resume'])   ? $_POST['$resume'] : null;

$return = [];

switch($method):

    case 'getCategory':

        $rows = select('materials', 'id, name', ' WHERE id NOT IN (SELECT material_id FROM material_relations)');

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'getMaterial':

        $rows = select('materials mat', ' mat.id, mat.name',
            ' INNER JOIN material_relations rel ON (mat.id = rel.material_id)
                    WHERE rel.material_relation = ' . $material_id);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'addTheme':
        if(empty($material_id)):
            $return = ['success' => false, 'message' => 'Informe a matéria!'];
        elseif(empty($theme)):
            $return = ['success' => false, 'message' => 'Informe o tema!'];
        else:
            // Add new concourse in  table @ materials
            $table = "themes";
            $values = [
                'material_id' => $material_id,
                'name' => $theme,
                'resume' => $resume,
            ];

            $theme_id = insert($table, $values);
           
            $return = ['success' => true, 'message' => 'Tema incluído com sucesso!', 'id' => $theme_id];
        endif;
        break; 
    case 'getThemes':
        // Select all concourses
        $table = "themes t";
        $values = "t.name as theme, c.name as category, m.name as material, s.corrects, s.incorrects, round(s.corrects*100/(s.corrects+s.incorrects),2) as note, TIMEDIFF(s.dh_finish, s.dh_start) as duration";
        $extra = " INNER JOIN materials m ON (t.material_id = m.id)
                   INNER JOIN material_relations r ON (m.id = r.material_id)
                   INNER JOIN materials c ON (r.material_relation = c.id)
                   INNER JOIN studies s ON (t.id = s.theme_id)";

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;

        break;

endswitch;

echo json_encode($return);
