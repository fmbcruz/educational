<?php

/**
 *  Cursos
 */

// Require da classe de conexão
require ('../../libs/sql.php');

// Method
$method = isset($_POST['txtMethod']) ? $_POST['txtMethod'] : "";
// Variables
$material_id = isset($_POST['material'])   ? $_POST['material'] : null;
$study_id    = isset($_POST['id'])         ? $_POST['id'] : null;
$concourse   = isset($_POST['concourse'])  ? $_POST['concourse'] : null;
$theme       = isset($_POST['theme'])      ? $_POST['theme'] : null;
$review      = isset($_POST['review'])     ? base64_encode($_POST['review']) : null;
$category    = isset($_POST['category'])   ? $_POST['category'] : "";
$corrects    = isset($_POST['corrects'])   ? $_POST['corrects'] : "";
$incorrects  = isset($_POST['incorrects']) ? $_POST['incorrects'] : "";
$save        = isset($_POST['save'])       ? $_POST['save'] : "";

$return = [];

switch($method):

    case 'getAllStudies':
        // Select all studies
        $table = "studies s";
        $values = "s.id, t.name as theme, c.name as category, m.name as material, TIMEDIFF(dh_finish, dh_start) as duration, DATE_FORMAT(s.dh_start,'%d/%m/%Y %H:%i') as dh_start, s.corrects, s.incorrects, round((corrects/(corrects + incorrects)*100),1) as note";
        $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                   INNER JOIN materials m ON (t.material_id = m.id)
                   INNER JOIN material_relations r ON (m.id = r.material_id)
                   INNER join materials c ON (r.material_relation = c.id)
                   WHERE s.dh_finish IS NOT NULL";

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há estudos cadastrados.', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;

        break;
    case 'getAllStudiesOfCategory':
        // Select all concourses
        if(empty($category)):
            $return = ['success' => false, 'message' => 'Informe a categoria!'];
        else:
            $table = "studies s";
            $values = "s.id, t.name as theme, c.name as category, m.name as material, TIMEDIFF(dh_finish, dh_start) as duration, DATE_FORMAT(s.dh_start,'%d/%m/%Y %H:%i') as dh_start, s.corrects, s.incorrects, round((corrects/(corrects + incorrects)*100),1) as note";
            $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                       INNER JOIN materials m ON (t.material_id = m.id)
                       INNER JOIN material_relations r ON (m.id = r.material_id)
                       INNER join materials c ON (r.material_relation = c.id)
                       WHERE s.dh_finish IS NOT NULL AND c.id = " . $category;

            $rows = select($table, $values, $extra);

            if(count($rows) <= 0):
                $return = ['success' => false, 'message' => 'Não há estudos cadastrados para esta categoria.', 'data' => $rows];
            else:
                $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
            endif;
        endif;
        break;

    case 'getConcourses':

        $rows = select('concourses', "id, CONCAT(DATE_FORMAT(date_exam, '%Y'), ' - ', company) AS name");

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Concursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'getCategory':

        $rows = select('materials', 'id, name', ' WHERE id NOT IN (SELECT material_id FROM material_relations)');

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'getMaterial':

        $rows = select('materials mat', ' mat.id, mat.name',
            ' INNER JOIN material_relations rel ON (mat.id = rel.material_id)
                    WHERE rel.material_relation = ' . $material_id);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'getTheme':

        $rows = select('themes', ' id, name',
            ' WHERE material_id = ' . $material_id);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'getOpenStudies':

        $table = "studies s";
        $values = "s.id, s.concourse_id, c.name as category, c.id as category_id, m.name as material, m.id as material_id,
                   s.theme_id, DATE_FORMAT(s.dh_start,'%d/%m/%Y %H:%i:%s') as dh_start,t.name as theme, s.corrects, s.incorrects,
                   DATE_FORMAT(s.dh_finish,'%d/%m/%Y %H:%i:%s') as dh_finish, FROM_BASE64(s.review) as review";
        $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                   INNER JOIN materials m ON (t.material_id = m.id)
                   INNER JOIN material_relations r On (m.id = r.material_id)
                   INNER JOIN materials c ON (r.material_relation = c.id)
                   WHERE dh_finish IS NULL";

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'getStudiesForEdition':

        $table = "studies s";
        $values = "s.id, s.concourse_id, c.name as category, c.id as category_id, m.name as material, m.id as material_id,
                   s.theme_id, DATE_FORMAT(s.dh_start,'%d/%m/%Y %H:%i:%s') as dh_start,t.name as theme, s.corrects, s.incorrects,
                   DATE_FORMAT(s.dh_finish,'%d/%m/%Y %H:%i:%s') as dh_finish, FROM_BASE64(s.review) as review";
        $extra = " INNER JOIN themes t ON (s.theme_id = t.id)
                   INNER JOIN materials m ON (t.material_id = m.id)
                   INNER JOIN material_relations r On (m.id = r.material_id)
                   INNER JOIN materials c ON (r.material_relation = c.id)
                   WHERE s.id = " . $study_id;

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;
        break;

    case 'startStudy':
        if(empty($theme)):
            $return = ['success' => false, 'message' => 'Informe o Tópico!'];
        else:
            // Add new concourse in  table @ materials
            $table = "studies";
            $values = [
                'theme_id' => $theme,
                'dh_start' => date("Y-m-d H:i:s")
            ];

            $study_id = insert($table, $values);
           
            $return = ['success' => true, 'message' => 'Estudo iniciado. Bons Estudos!', 'id' => $study_id];
        endif;
        break;

    case 'stopStudy':
        if(empty($theme)):
            $return = ['success' => false, 'message' => 'Informe o Tópico!'];
        else:

            if(empty($corrects) || empty($incorrects)):
                $corrects = 0;
                $incorrects = 0;
            endif;

            // Add new concourse in  table @ materials
            $table = "studies";
            if(empty($save)):
                $columns = 'theme_id = ?, concourse_id = ?, dh_finish = ?, review = ?, corrects = ?, incorrects = ?';
                $values = [
                    'theme_id'  => $theme,
                    'concourse_id'  => $concourse,
                    'dh_finish' => date("Y-m-d H:i:s"),
                    'review'    => $review,
                    'corrects'      => $corrects,
                    'incorrects'    => $incorrects,
                ];
            else:
                $columns = 'theme_id = ?, concourse_id = ?, dh_update = ?, review = ?, corrects = ?, incorrects = ?';
                $values = [
                    'theme_id'      => $theme,
                    'concourse_id'  => $concourse,
                    'dh_update'     => date("Y-m-d H:i:s"),
                    'review'        => $review,
                    'corrects'      => $corrects,
                    'incorrects'    => $incorrects,
                ];
            endif;

            $study_id = update($table,$columns, $values, 'id = ' . $study_id);
            $return = ['success' => true, 'message' => 'Estudo finalizado. Parabéns!', 'id' => $study_id];
        endif;
        break;

    case 'getThemes':
        // Select all concourses
        $table = "themes t";
        $values = "t.name as theme, c.name as category, m.name as material";
        $extra = " INNER JOIN materials m ON (t.material_id = m.id)
                   INNER JOIN material_relations r ON (m.id = r.material_id)
                   INNER join materials c ON (r.material_relation = c.id)";

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Cursos selecionados', 'data' => $rows];
        endif;

        break;

    case 'autoUpdateReview':
        // Automatic update review
        $table = "studies";
        $columns = "review = ?";
        $values = [
            "review" => $review,
        ];

        $study_id = update($table,$columns, $values, 'id = ' . $study_id);
        $return = ['success' => true, 'message' => 'Texto salvo!', 'id' => $study_id];

        break;

endswitch;

echo json_encode($return);
