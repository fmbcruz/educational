<?php

/**
 *  Cursos
 */

// Require da classe de conexão
require ('../../libs/sql.php');

// Table
$table = "concourses";
// Method
$method = isset($_POST['txtMethod']) ? $_POST['txtMethod'] : "";
// Variables
$id               = isset($_POST['id'])   ? $_POST['id'] : null;
$company          = isset($_POST['company'])   ? $_POST['company'] : null;
$dateSubscription = isset($_POST['dateSubscription']) ? $_POST['dateSubscription'] : null;
$dateExam         = isset($_POST['dateExam']) ? $_POST['dateExam'] : null;
$hourExam         = isset($_POST['hourExam']) ? $_POST['hourExam'] : null;
$job              = isset($_POST['job']) ? $_POST['job'] : null;
$salary           = isset($_POST['salary']) ? $_POST['salary'] : null;
$examBoard        = isset($_POST['examBoard']) ? $_POST['examBoard'] : null;
$infoURL          = isset($_POST['infoURL']) ? $_POST['infoURL'] : null;
$subscriptionValue= isset($_POST['subscriptionValue']) ? $_POST['subscriptionValue'] : null;
$subscription     = isset($_POST['subscription']) ? 1 : null;

$return = [];
$extra = '';

if($hourExam !== null):
    $dateExam = $dateExam . ' ' . $hourExam;
endif;

switch($method):

    case 'insert':
        if(empty($company)):
            $return = ['success' => false, 'message' => 'Informe o nome do concurso!'];
        elseif(empty($dateSubscription)):
            $return = ['success' => false, 'message' => 'Informe a data de inscrição!'];
        elseif(empty($dateExam)):
            $return = ['success' => false, 'message' => 'Informe a data da prova!'];
        elseif(empty($job)):
            $return = ['success' => false, 'message' => 'Informe o cargo!'];
        elseif(empty($salary)):
            $return = ['success' => false, 'message' => 'Informe o salário!'];
        else:
            $values = [
                'company' => $company,
                'date_subscription' => $dateSubscription,
                'date_exam' => $dateExam,
                'subscription_value' => $subscriptionValue,
                'subscription' => $subscription,
                'job' => $job,
                'exam_board' => $examBoard,
                'salary' => $salary,
                'url' => $infoURL,
            ];

            $concourse_id = insert($table, $values);

            $return = ['success' => true, 'message' => 'Concurso incluído com sucesso!', 'id' => $concourse_id];
        endif;
        break;
    break;

    case 'update':
        if(empty($id)):
            $return = ['success' => false, 'message' => 'Informe o id!'];
        elseif(empty($company)):
            $return = ['success' => false, 'message' => 'Informe o nome do concurso!'];
        elseif(empty($dateSubscription)):
            $return = ['success' => false, 'message' => 'Informe a data de inscrição!'];
        elseif(empty($dateExam)):
            $return = ['success' => false, 'message' => 'Informe a data da prova!'];
        elseif(empty($job)):
            $return = ['success' => false, 'message' => 'Informe o cargo!'];
        elseif(empty($salary)):
            $return = ['success' => false, 'message' => 'Informe o salário!'];
        else:
            $columns = "company = ?, date_subscription = ?, date_exam = ?, subscription_value = ?, subscription = ?, job = ?, exam_board = ?, salary = ?, url = ?";
            $values = [
                'company' => $company,
                'date_subscription' => $dateSubscription,
                'date_exam' => $dateExam,
                'subscription_value' => $subscriptionValue,
                'subscription' => $subscription,
                'job' => $job,
                'exam_board' => $examBoard,
                'salary' => $salary,
                'url' => $infoURL,
            ];

            $study_id = update($table,$columns, $values, 'id = ' . $id);
            $return = ['success' => true, 'message' => 'Concurso editado com sucesso!', 'id' => $id];
        endif;
        break;

    case 'select':
        $values = "id, company, DATE_FORMAT(date_subscription,'%d/%m/%Y') as dh_subscription,
        DATE_FORMAT(date_exam,'%d/%m/%Y') as dh_exam, DATE_FORMAT(date_subscription,'%Y-%m-%d') as dh_subscription_edit,
        DATE_FORMAT(date_exam,'%Y-%m-%d') as dh_exam_edit, DATE_FORMAT(date_exam,'%H:%i') as hour_exam, job, round(salary,2) as salary, round(subscription_value,2) as subscription_value, exam_board, url, subscription";

        if(!empty($id)):
            $extra = " WHERE id = " . $id;
        endif;

        $rows = select($table, $values, $extra);

        if(count($rows) <= 0):
            $return = ['success' => false, 'message' => 'Não há registros cadastrados', 'data' => $rows];
        else:
            $return = ['success' => true, 'message' => 'Concursos selecionados', 'data' => $rows];
        endif;

        break;

    case 'delete':
        if(empty($id)):
            $return = ['success' => false, 'message' => 'Informe o id!'];
        else:

            $resp = delete($table, 'id = ' . $id);
            $return = ['success' => true, 'message' => 'Concurso apagado com sucesso!', 'id' => $resp];
        endif;
        break;



endswitch;

echo json_encode($return);

