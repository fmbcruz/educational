<?php

date_default_timezone_set('America/Sao_Paulo');

/**
* @ Upload essentials file0asdfgui
 *
* @ author: Filipe Cruz
* @ email: fmbcruz@live.com
*/
    /**
     * @ Include routes to use function getUrl();
     */
    include_once 'libs/routes.php';

    /**
     * @ Include respective view
     */
    include_once getView();
